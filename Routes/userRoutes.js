const express=require('express');
const userServices=require('../services/userService')
const auth=require('../middleware/auth');
const routes=express.Router()



 
//--profile Api
routes.post('/signUp',userServices.signUpByUser)
routes.post('forgotPassword',userServices.forgetPassword)
routes.post('/setNewPassword',userServices.setNewPassword)
routes.post('/LogIn',userServices.logInByUser)
routes.get('/getProfile',auth.userVerify,userServices.getProfile)
routes.put('/change-Password',auth.userVerify,userServices.changePassword)
routes.delete('/delete-Profile',auth.userVerify,userServices.deleteProfile)

routes.post('/bookAppointment',auth.userVerify,userServices.appointment)

module.exports=routes;
