const express=require('express')
const routes=express.Router()
const doctor=require('../Routes/doctorRoutes')
const user=require('../Routes/userRoutes')

routes.use('/doctor',doctor)
routes.use('/user',user)

module.exports=routes;