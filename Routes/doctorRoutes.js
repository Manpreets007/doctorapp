const express = require('express');
const router = express.Router()
const doctorService = require('../services/doctorService');
const auth = require('../middleware/auth');
const routes = require('./userRoutes');



//Admin Login Form

router.post('/doctorSignUp', doctorService.signUpByDoctor)
router.post('/forget-Password', doctorService.forgetPasswordAdd)
router.put('/setNewPassword', doctorService.setNewPasswordAdd)
router.post('/doctorLogIn', doctorService.logInByDoctor)
router.get('/doctorProfile', auth.doctorVerify, doctorService.doctorProfile)
router.delete('/delete', auth.doctorVerify, doctorService.remove)

router.post('./confirmedAppointment',auth.doctorVerify,doctorService.appointmentConfirmation)



module.exports = router;