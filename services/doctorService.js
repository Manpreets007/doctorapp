const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const doctorSchema = require('../Model/doctorSchema')
const userSchema = require('../Model/usersSchema')
const appointmentSchema = require('../Model/appointmentSchema')
const otp = require('../Model/otpSchema')
const nodeMailer=require('nodemailer')
const schedule = require('node-schedule');
const cron=require('node-cron')
const auth=require('../middleware/auth')

//---- doctorProfile-apis

exports.signUpByDoctor = async (req, res) => {
  try {
    const thereIsAdmin = await doctorSchema.findOne({ email: req.body.email })
    if (thereIsAdmin) {
      console.log("!!!!!!!!!!!!!")
      return res.status(400).send({
        msg: 'user another username',
        data: {}

      })
    }
    const password = await bcrypt.hash(req.body.password, 10)
    const doctor = await doctorSchema.create({
      doctorName: req.body.doctorName, email: req.body.email, password: password
    })

    console.log('done!!!!!!!!!!!')
    return res.status(200).send({
      msg: 'doctor created',
      data: doctor
    })
  } catch (err) {
    return res.status(400).send({
      msg: err
    })
  }
}
exports.forgetPasswordAdd = async (req, res) => {
  try {
    const user = await doctorSchema.findOne({ email: req.body.email })
    if (user) {
      const createotp = await Math.floor(Math.random() * 1000000 + 1)
      let randomOtp = {
        email: req.body.email,
        code: createotp,
        expaireIn: new Date().getTime() + 400 * 10000
      }
      console.log(randomOtp, 'fjgjhsdijgig')
      const sentOtp = await otp.create(randomOtp)
      return res.status(200).send({
        msg: 'OTP sent to You Succefully',
        data: sentOtp
      })

    }
  } catch (err) {

  }
}
exports.setNewPasswordAdd = async (req, res) => {
  try {
    const data = await otp.findOne({ email: req.body.email, code: req.body.code })
    if (data) {
      console.log(data)
      let time = new Date().getTime();

      let pendingTime = Number(data.expaireIn - time)
      console.log(pendingTime, "?>>>>.")
      if (pendingTime > 0) {

        const Password = await bcrypt.hash(req.body.password, 10)

        const NewPassword = await doctorSchema.findOneAndUpdate({ email: req.body.email }, { password: Password }, { upsert: true, new: true })

        let oldotp = await otp.findOneAndDelete({ code: req.body.code })
        console.log(oldotp)

        return res.send({
          msg: 'password change successfully',
          data: NewPassword
        })

      } else {
        return res.send('Otp expire')
      }
    }
    else {
      return res.send('invaild code')
    }

  } catch (err) {
    return res.status(400).send({
      msg: "err",
      data: err
    })
  }
}
exports.logInByDoctor = async (req, res) => {
  try {
    const admin = await doctorSchema.findOne({ userName: req.body.userName })
    if (admin) {
      const compare = await bcrypt.compare(req.body.password, admin.password)
      console.log(compare, '?????????????/')
      if (compare) {
        const match = {}
        const token = await jwt.sign({ _id: admin._id }, 'vdffuvsdufhbbvvgsu')
        match.admin = admin;
        match.token = token;
        console.log('status Okkk$$$$$');
        return res.status(200).send({
          msg: 'you logged successfully',
          data: match
        })
      }
    } return res.status(400).send({
      msg: 'userName or password',
      data: {}
    })
  } catch (err) {
    return res.status(400).send({
      msg: 'try again',
      data: err
    })
  }

}
exports.doctorProfile = async (req, res) => {
  try {
    const check = await doctorSchema.findById(req.admin._id)
    return res.status(200).send({
      msg: 'welcome to your profile',
      data: check
    })
  } catch (err) {
    return err
  }
}
exports.remove = async (req, res) => {
  try {
    const deleteProfile = await doctorSchema.findByIdAndDelete({ _id: req.body._id })
    return res.status(200).send({
      msg: 'your account deleted',
      data: deleteProfile
    })

  }
  catch (err) {
    return res.send({
      data: err
    })
  }
}


exports.appointmentConfirmation = async (req, res) => {
  try {
    const check = await appointmentSchema.findById({ _id: req.body.id })
    if (check) {
      if (req.body.status == 'appointment_booked') {
        let Data = await appointmentModel.findOneAndUpdate({ _id: find._id }, { $push:{ status: 'appointment_booked' } }, { new: true })
        return res.status(201).send({
          msg: "your appointment is confirmed",
          data: Data
        })
      }
      if (req.body.status =='cancel') {
        let Data = await appointmentModel.findOneAndUpdate({ _id: find._id }, { $push:{ status: 'cancel' } }, { new: true })
        return res.send({
          msg: "pick another date this slot allready booked",
          data: Data
        })
      }
    }}catch (err) {
        return res.send({
          data: err
        })
      }
    }

    
exports.scheduleSender=async(req,res)=>{
try {   
  let check =await appointmentSchema.find({userId:req.body.userId})
  if(check){

  
     cron.schedule('*,*,24,*,*,*',()=>{
     let mailOptions={
        from:"Manpreet321@gmail.com",
        to:"abc123@gmail.com",
        subject:" your appiontment ",
        text:"hello your appiontment is tomorrow"
      }
      let transporter=nodeMailer.createTransport({
        service:'email',
        auth:auth.doctorVerify
       })
       transporter.sendMail(mailOptions,(error, info)=>{
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
    });
           
      
    })
  }
  } catch(error){
    return res.send(error)
}}
