const express=require('express')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const userSchema =require('../Model/usersSchema')
const appointmentSchema=require('../Model/appointmentSchema')
const doctorSchema = require('../Model/doctorSchema')



//logIn api of user

exports.signUpByUser = async (req, res) => {
    
    try {
        const thereIsOldUser = await userSchema.findOne({ email: req.body.email })
        if (thereIsOldUser) {
            console.log(thereIsOldUser, '??????????????')
            return res.status(409).send({
                msg: 'use another Username',
                data: {}
            })
        }
        const password = await bcrypt.hash(req.body.password, 10)
        const user = {
            email: req.body.email,
            password: password,
            address:req.body.address,
            phone:req.body.phone,
            fullname: req.body.fullname
        }

        const newUser = await userSchema.create(user)
        return res.status(201).send({
            msg: 'user created',
            data: newUser
        })
    } catch (err) {
        return res.status(400).send({
            msg: 'err',
            data: err
        })
    }
}
exports.logInByUser = async (req, res) => {
    try {
        const user = await userSchema.findOne({ email: req.body.email })
        if (user) {
            const compare = await bcrypt.compare(req.body.password, user.password)
            if (compare) {
                const match = {}
                const token = await jwt.sign({ _id: user._id }, 'hfigdgydyayfsdngdsy')
                match.token = token;
                match.user = user;
                return res.status(200).send({
                    msg: 'you logged successfully',
                    data: match
                })
            }
        }
        return res.status(400).send({
            msg: 'fill a vaild email or password',
            data: {}
        })
    }
    catch (err) {
        return res.status(400).send({
            msg: 'something worng',
            data: err
        })
    }
}
exports.forgetPassword = async (req, res) => {
    try {
        const user = await userSchema.findOne({ email: req.body.email })
        if (user) {
            const createotp = await Math.floor(Math.random() * 1000000 + 1)
            let randomOtp = {
                email: req.body.email,
                Code: createotp,
                expireIn: new Date().getTime() + 20 * 10000
            }
            console.log(randomOtp, 'fjgjhsdijgig')

            const sentOtp = await otp.create(randomOtp)
            return res.status(200).send({
                msg: 'OTP sent to You Succefully',
                data: sentOtp
            })

        }
    } catch (err) {

    }
}
exports.setNewPassword = async (req, res) => {
    try {
        const otp = await otp.findOne({ email: req.body.email, code: req.body.code })
        if (otp) {
            let time = new Date().getTime();
            let pendingTime = otp.expireIn - time
            if (pendingTime > 0) {
                const NewPassword = await findOneAndUpdate({ email: req.body.email }, { password: req.body.password })
                const oldotp = await otp.findOneAndDelete({ code: req.body.code })
                return res.send({
                    msg: 'password change successfully',
                    data: NewPassword
                })

            }
        }

    } catch (err) {
        return res.status(400).send({
            msg: "err",
            data: err
        })
    }
}
exports.getProfile = async (req, res) => {
    try {
        const user = await userSchema.findById(req.user._id)
        console.log(user, '!!!!!!!!!!!!!!!!!!!!!!!!')
        if (user) {
            return res.status(200).send({
                msg: 'welcome to profile',
                data: user
            })
        }
        return res.status(400).send({
            msg: "User not Found",
            data: {}
        })
    } catch (error) {

        return res.status(400).send({
            msg: "error",
            data: error
        })

    }
}
exports.changePassword = async (req, res) => {
    try {
        const User = await userSchema.findById(req.user._id)
        if (User) {
            const NewPassword = await userSchema.findOneAndUpdate({ email: req.body.email },
                { password: req.body.password }, { upsert: true, new: true })
            return res.status(200).send({
                msg: "resetPassword success",
                data: NewPassword

            })
        }
        return res.status(409).send({
            msg: 'put right email',
            data: {}
        })
    } catch (err) {
        return res.status(400).send({
            msg: 'something is not right',
            data: err
        })
    }
}
exports.deleteProfile = async (req, res) => {
    try {
        const checkUser = await userSchema.findById(req.user._id)
        if (checkUser) {
            console.log(checkUser, "%%%%%%%%%%%%%%")
            const Remove = await userSchema.findOneAndDelete({ email: req.body.email }, { id: req.body.id })
            console.log(Remove)
            return res.status(200).send({
                msg: 'your id delete',
                data: Remove
            })
        } return res.status(401).send({
            msg: 'user not found',
            data: {}
        })
    } catch (error) {
        return res.status(400).send({
            msg: 'err',
            data: error
        })

    }
}



// for user appointment

exports.appointment=async(req,res,next)=>{
    try {
         let check=await appointmentSchema.findOne({_id:req.body.doctorId })
         if(check){  
                let data = {
                    userId:req.user,
                    doctorId:req.body.doctorId,
                    date:new Date().getTime().getDate().add()
                }
                const confrim = await appointmentSchema.create(data)
                return res.status(200).send({
                    msg:"wait for confrmation",
                    data:confrim
                })
                }return res.send('data not found')
         
           }   catch (error) {
               return res.next(error)
        
    }
}



// exports.querySenderToDoctor=async(req,res,next)=>{
//     try {
//          let querysOfUser={email:req.query.email,
//                          name:req.query.name,
//                       text:req.query.text
//                     }
//          let check=await doctorSchema.insert(querysOfUser)
//     } catch (error) {
//         return res.next(err)
//     }
// }