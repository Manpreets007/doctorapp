const express=require('express')
const mongoose=require('mongoose')
const connection=require('mongodb')
const routes=require('./Routes/index')
const socketService=require('./services/socketServices')
const url='mongodb://127.0.0.1:27017/DoctorApp'
const app=express()
const port=3000
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
socketService.init(io)
app.use(express.json())
app.use('/',routes)


app.use((err, req, res, next) => {
    console.log(err);
    return res.send({
        error: err
    })
})

mongoose.connect(url)

const db=mongoose.connection
db.once('open',_=>{
   console.log("database Connected :",url)
})
db.on('err',err=>{
    console.log('connction error:',err)
})



server.listen(port,()=>{
    console.log(`listening the port at:${port}`)
})