const mongoose=require('mongoose')

const appointmentSchema= mongoose.Schema({
    userId:{
        type:mongoose.Types.ObjectId,
        required:true
    },
    doctorId:{
        type:mongoose.Types.ObjectId,
        required:true
    },
    date:{
        type:Date
    },
    status:{
        type:String,
        default:'pending'
    }

})

module.exports=mongoose.model('appointment',appointmentSchema)