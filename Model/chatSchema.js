const mongoose=require('mongoose')
const messageSchema = new Schema({
  user_name:{
    type: String,
    required: true
  },
  message_text: {
    type: String,
    required: true
  },images:{
    type:Image
  }
},{
  timestamps: {
    createdAt: true,
    updatedAt: false
  }
})